// ===== FILTERS TABS =====
const tabs = document.querySelectorAll('[data-target]'),
      tabContents = document.querySelectorAll('[data-content]')

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        const target = document.querySelector(tab.dataset.target)

        tabContents.forEach(tc => {
            tc.classList.remove('filters__active')
        })
        target.classList.add('filters__active')

        tabs.forEach(t => {
            t.classList.remove('filters-tab-active')
        })
        tab.classList.add('filters-tab-active')
    })
})

// ===== DARK THEME =====
const themeButton = document.getElementById('theme-button')
const darkTheme = 'dark-theme'
const lightTheme = 'light-theme'
const iconThemes = {
    'dark-theme' : 'ri-sun-line',
    'light-theme' : 'ri-moon-line'
}

const selectedTheme = localStorage.getItem('selected-theme')
const selectedIcon = localStorage.getItem('selected-icon')

const getCurrentTheme = () => document.body.classList.contains(lightTheme)? lightTheme : darkTheme
const getCurrentIcon = () => themeButton.classList.contains(iconThemes[lightTheme])? iconThemes[lightTheme] : iconThemes[darkTheme]

if (selectedTheme) {
    document.body.classList[selectedTheme === lightTheme? 'add' : 'remove'](lightTheme)
    themeButton.classList[selectedIcon === iconThemes[lightTheme]? 'add' : 'remove'](iconThemes[lightTheme])
    themeButton.classList[selectedIcon === iconThemes[lightTheme]? 'remove' : 'add'](iconThemes[darkTheme])
}

themeButton.addEventListener('click', () => {
    document.body.classList.toggle(lightTheme)
    themeButton.classList.toggle(iconThemes[lightTheme])
    themeButton.classList.toggle(iconThemes[darkTheme])

    localStorage.setItem('selected-theme', getCurrentTheme())
    localStorage.setItem('selected-icon', getCurrentIcon())
})

// ===== ScrollReveal =====
const sr = ScrollReveal( {
    origin: 'top',
    distance: '60px',
    duration: 2500,
    delay: 400,
})

sr.reveal('header')
sr.reveal('.profile__border')
sr.reveal('.profile__name', {delay: 500})
sr.reveal('.profile__profession', {delay: 600})
sr.reveal('.profile__social', {delay: 700})
sr.reveal('.profile__about', {interval:100, delay: 700})
sr.reveal('.profile__cv', {delay: 800})
sr.reveal('.filters__content', {delay: 900})
sr.reveal('.filters', {delay: 1000})
sr.reveal('.footer', {delay: 1000})